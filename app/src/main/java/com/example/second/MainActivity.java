package com.example.second;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;


import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private final MainActivity viewBinding = this;
    private final ArrayList<Double> operands = new ArrayList<>();

    private String usingOperator = null;

    private TextView inputTextView;
    private TextView outputTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LinearLayout mainContainer = new LinearLayout(this);
        mainContainer.setOrientation(LinearLayout.VERTICAL);

        final int dimen16dp = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 16, getResources().getDisplayMetrics());

        inputTextView = new TextView(this);
        inputTextView.setGravity(Gravity.END);
        inputTextView.setPadding(dimen16dp, 0, dimen16dp, 0);
        inputTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 32);

        outputTextView = new TextView(this);
        outputTextView.setGravity(Gravity.END);
        outputTextView.setPadding(dimen16dp, 0, dimen16dp, 0);
        outputTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 32);


        final LinearLayout.LayoutParams textViewParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0, 1);
        mainContainer.addView(inputTextView, textViewParams);
        mainContainer.addView(outputTextView, textViewParams);

        //кнопки + строки
        TextView buttonClear = createButton(ContextCompat.getDrawable(this, R.drawable.ripple_dark_grey), "C", ContextCompat.getColor(this, R.color.white));
        TextView buttonBrackets = createButton(ContextCompat.getDrawable(this, R.drawable.ripple_dark_grey), "(  )", ContextCompat.getColor(this, R.color.white));
        TextView buttonPercent = createButton(ContextCompat.getDrawable(this, R.drawable.ripple_dark_grey), "%", ContextCompat.getColor(this, R.color.white));
        TextView buttonDivision = createButton(ContextCompat.getDrawable(this, R.drawable.ripple_dark_grey), "/", ContextCompat.getColor(this, R.color.white));
        LinearLayout row1 = createRow(new TextView[]{buttonClear, buttonBrackets, buttonDivision, buttonPercent});

        TextView buttonSeven = createButton(ContextCompat.getDrawable(this, R.drawable.ripple_grey), "7", ContextCompat.getColor(this, R.color.black));
        TextView buttonEight = createButton(ContextCompat.getDrawable(this, R.drawable.ripple_grey), "8", ContextCompat.getColor(this, R.color.black));
        TextView buttonNine = createButton(ContextCompat.getDrawable(this, R.drawable.ripple_grey), "9", ContextCompat.getColor(this, R.color.black));
        TextView buttonPlus = createButton(ContextCompat.getDrawable(this, R.drawable.ripple_dark_grey), "/", ContextCompat.getColor(this, R.color.white));
        LinearLayout row2 = createRow(new TextView[]{buttonSeven, buttonEight, buttonNine, buttonPlus});

        TextView buttonSix = createButton(ContextCompat.getDrawable(this, R.drawable.ripple_grey), "6", ContextCompat.getColor(this, R.color.black));
        TextView buttonFive = createButton(ContextCompat.getDrawable(this, R.drawable.ripple_grey), "5", ContextCompat.getColor(this, R.color.black));
        TextView buttonFour = createButton(ContextCompat.getDrawable(this, R.drawable.ripple_grey), "4", ContextCompat.getColor(this, R.color.black));
        TextView buttonMinus = createButton(ContextCompat.getDrawable(this, R.drawable.ripple_dark_grey), "-", ContextCompat.getColor(this, R.color.white));
        LinearLayout row3 = createRow(new TextView[]{buttonSix, buttonFive, buttonFour, buttonMinus});

        TextView buttonOne = createButton(ContextCompat.getDrawable(this, R.drawable.ripple_grey), "1", ContextCompat.getColor(this, R.color.black));
        TextView buttonTwo = createButton(ContextCompat.getDrawable(this, R.drawable.ripple_grey), "2", ContextCompat.getColor(this, R.color.black));
        TextView buttonThree = createButton(ContextCompat.getDrawable(this, R.drawable.ripple_grey), "3", ContextCompat.getColor(this, R.color.black));
        TextView buttonMultiplication = createButton(ContextCompat.getDrawable(this, R.drawable.ripple_dark_grey), "x", ContextCompat.getColor(this, R.color.white));
        LinearLayout row4 = createRow(new TextView[]{buttonOne, buttonTwo, buttonThree, buttonMultiplication});

        TextView buttonEqually = createButton(ContextCompat.getDrawable(this, R.drawable.ripple_orange), "=", ContextCompat.getColor(this, R.color.white));
        TextView buttonSign = createButton(ContextCompat.getDrawable(this, R.drawable.ripple_grey), "+-", ContextCompat.getColor(this, R.color.black));
        TextView buttonZero = createButton(ContextCompat.getDrawable(this, R.drawable.ripple_grey), "0", ContextCompat.getColor(this, R.color.black));
        TextView buttonDot = createButton(ContextCompat.getDrawable(this, R.drawable.ripple_grey), ".", ContextCompat.getColor(this, R.color.black));
        LinearLayout row5 = createRow(new TextView[]{buttonDot, buttonZero, buttonSign, buttonEqually});

        //добавляем на экран
        mainContainer.addView(row1);
        mainContainer.addView(row2);
        mainContainer.addView(row3);
        mainContainer.addView(row4);
        mainContainer.addView(row5, textViewParams);
        setContentView(mainContainer);

        //слушатели
        buttonEqually.setOnClickListener(this::handleEquallyClick);
        buttonClear.setOnClickListener(this::handleClearClick);
        buttonPlus.setOnClickListener(this::handleOperatorClick);
        buttonMinus.setOnClickListener(this::handleOperatorClick);
        buttonDivision.setOnClickListener(this::handleOperatorClick);
        buttonMultiplication.setOnClickListener(this::handleOperatorClick);

        buttonDot.setOnClickListener(this::handleClickDot);
        buttonZero.setOnClickListener(this::handleClickNum);
        buttonOne.setOnClickListener(this::handleClickNum);
        buttonTwo.setOnClickListener(this::handleClickNum);
        buttonThree.setOnClickListener(this::handleClickNum);
        buttonFour.setOnClickListener(this::handleClickNum);
        buttonFive.setOnClickListener(this::handleClickNum);
        buttonSix.setOnClickListener(this::handleClickNum);
        buttonSeven.setOnClickListener(this::handleClickNum);
        buttonEight.setOnClickListener(this::handleClickNum);
        buttonNine.setOnClickListener(this::handleClickNum);
    }


    public TextView createButton(Drawable backgroundColor, String buttonText, int textColor) {
        final LinearLayout.LayoutParams buttonParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, 1);
        final TextView button = new TextView(this);
        button.setBackground(backgroundColor);
        button.setText(buttonText);
        button.setTextColor(textColor);
        button.setGravity(Gravity.CENTER);
        button.setTextSize(TypedValue.COMPLEX_UNIT_SP, 32);
        button.setLayoutParams(buttonParams);
        return button;
    }

    public LinearLayout createRow(TextView[] buttonsArr) {
        final LinearLayout row = new LinearLayout(this);
        row.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 0, 1));

        for (TextView button : buttonsArr) {
            row.addView(button);
        }

        return row;
    }

    private void handleClickDot(View view) {
        String currentOperand = viewBinding.inputTextView.getText().toString();
        if (!currentOperand.isEmpty()) {
            if (!currentOperand.contains(".")) {
                viewBinding.inputTextView.setText(currentOperand.concat(((TextView) view).getText().toString()));
            }

        } else {
            viewBinding.inputTextView.setText("0.");
        }

    }


    private void handleEquallyClick(View view) {
        if (!"".equals(usingOperator) && !"".equals(viewBinding.inputTextView.getText().toString())) {
            operands.add(Double.parseDouble(viewBinding.inputTextView.getText().toString()));
            viewBinding.outputTextView.setText(getResult(usingOperator));
            viewBinding.inputTextView.setText("");
        }
    }

    private void handleClearClick(View view) {
        operands.clear();
        usingOperator = "";
        viewBinding.inputTextView.setText("");
        viewBinding.outputTextView.setText("");
    }


    private void handleOperatorClick(View view) {

        if (!"".equals(viewBinding.inputTextView.getText().toString())) {
            operands.add(Double.parseDouble(viewBinding.inputTextView.getText().toString()));

            if (usingOperator != null) {
                String result = getResult(usingOperator);
                viewBinding.outputTextView.setText(result);
            }
        }
        usingOperator = ((TextView) view).getText().toString();
        viewBinding.inputTextView.setText("");
    }


    private void handleClickNum(View view) {
        if ("0".equals(viewBinding.inputTextView.getText().toString())) {
            viewBinding.inputTextView.setText(((TextView) view).getText().toString());
        } else {
            final String text = viewBinding.inputTextView
                    .getText()
                    .toString()
                    .concat(((TextView) view).getText().toString());
            viewBinding.inputTextView.setText(text);
        }

    }

    String getResult(String buttonValue) {
        String resultString = "";

        if (operands.size() == 2) {
            double result;

            System.out.println("Числа: " + operands + "Оператор: " + usingOperator);

            switch (usingOperator) {
                case ("+"):
                    result = operands.get(0) + operands.get(1);
                    resultString = Double.toString(result);
                    break;
                case ("-"):
                    result = operands.get(0) - operands.get(1);
                    resultString = Double.toString(result);
                    break;
                case ("x"):
                    if (operands.get(0) == 0.0 || operands.get(1) == 0.0) {
                        resultString = "0";
                    } else {
                        result = operands.get(0) * operands.get(1);
                        resultString = Double.toString(result);
                    }

                    break;
                case ("/"):

                    result = operands.get(0) / operands.get(1);
                    resultString = Double.toString(result);

                    break;
            }

            if (resultString.endsWith(".0")) {
                resultString = resultString.substring(0, resultString.length() - 2);
            }

            operands.clear();
            operands.add(Double.parseDouble(resultString));

            usingOperator = buttonValue;

        }
        return resultString;
    }

}